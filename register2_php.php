<!DOCTYPE HTML>  
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/mystyle-1.css">
</head>
</head>
<body>  

<?php
// define variables and set to empty values
$nameErr = $lnameErr = $emailErr = $genderErr = $websiteErr = $bday = "";
$name = $lname = $email = $gender = $comment = $website = $bdayErr = "";



if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if (empty($_POST["name"])) {
    $nameErr = "Name is required";

  }elseif(strlen($_POST["name"]) < 3  ) {
    $nameErr = "Name needs to have atleast 3 characters";
  }
 
   else {
    $name = test_input($_POST["name"]);
    // check if name only contains letters and whitespace
    if (!preg_match("/^[a-zA-Z ]*$/",$name)) {
      $nameErr = "Only letters and white space allowed"; 
    }
  }


    if (empty($_POST["lname"])) {
      $lnameErr = "Last name is required";
  
    }elseif(strlen($_POST["lname"]) < 3  ) {
      $lnameErr = "Last name needs to have atleast 3 characters";
    }
   
     else {
      $lname = test_input($_POST["lname"]);
      // check if last name only contains letters and whitespace
      if (!preg_match("/^[a-zA-Z ]*$/",$lname)) {
        $lnameErr = "Only letters and white space allowed"; 
      }
    }
  
  if (empty($_POST["email"])) {
    $emailErr = "Email is required";
  } else {
    $email = test_input($_POST["email"]);
    // check if e-mail address is well-formed
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $emailErr = "Invalid email format"; 
    }
    elseif(strlen(strstr($_POST["email"],'@', true)) < 6  ) {
      $emailErr = "Email need to have more than 6 characters";
    }
    elseif (checkEmailAdress($email)) {
      $emailErr = "Email needs to contain @gmail!";
    }

  }
    
  if (empty($_POST["website"])) {
    $website = "";
  } else {
    $website = test_input($_POST["website"]);
    // check if URL address syntax is valid (this regular expression also allows dashes in the URL)
    if (!preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$website)) {
      $websiteErr = "Invalid URL"; 
    }
  }

  if (empty($_POST["comment"])) {
    $comment = "";
  } else {
    $comment = test_input($_POST["comment"]);
  }
  if (empty($_POST["bday"])) {
    $bdayErr = "Date of birth is required";
  } else {
    $bday = test_input($_POST["bday"]);
  }

  if (empty($_POST["gender"])) {
    $genderErr = "Gender is required";
  } else {
    $gender = test_input($_POST["gender"]);
  }
  
  if (!empty($nameErr) or !empty($lnameErr) or !empty($emailErr) or !empty($websiteErr) or !empty($commentErr) or !empty($genderErr) or !empty($bdayErr) ) {
    $params = "name=" . urlencode($_POST["name"]);
    $params .= "&lname=" . urlencode($_POST["lname"]);
    $params .= "&email=" . urlencode($_POST["email"]);
    $params .= "&website=" . urlencode($_POST["website"]);
    $params .= "&comment=" . urlencode($_POST["comment"]);
    $params .= "&gender=" . urlencode($_POST["gender"]);
    $params .= "&bday=" . urlencode($_POST["bday"]);

    $params .= "&nameErr=" . urlencode($nameErr);
    $params .= "&lnameErr=" . urlencode($lnameErr);
    $params .= "&emailErr=" . urlencode($emailErr);
    $params .= "&websiteErr=" . urlencode($websiteErr);
    $params .= "&commentErr=" . urlencode($commentErr);
    $params .= "&genderErr=" . urlencode($genderErr);
    $params .= "&bdayErr=" . urlencode($bdayErr);
  
    header("Location: index2_php.php?" . $params);
 }   else {

    echo "<h2>Your Input:</h2>";
    echo "Name: " . $_POST['name'];
    echo "<br>";

    echo "Last name: " . $_POST['lname'];
    echo "<br>";
    
    echo "Email: " . $_POST['email'];
    echo "<br>";
    
    echo "Website: " . $_POST['website'];
    echo "<br>";
    $date = date_create ($bday);
    echo "Date of birth: " .date_format($date,"F j, l, Y");
    echo "<br>";
    
    echo "Comment: " . $_POST['comment'];
    echo "<br>";
    
    echo "Gender: " . $_POST['gender'];  
    echo "<br>";
    echo "<br>";
    
    echo "<a href=\"index2_php.php\">Return to form</a>";
  }
}

function test_input($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function checkEmailAdress($data) {
  if (!preg_match("/@gmail.com$/",$data) === true)
  return true;
  else
  return false;
}  
?>

</body>
</html>