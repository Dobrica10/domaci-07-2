<!DOCTYPE HTML>  
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>  

<?php
    $name = $lname = $email = $website = $comment = $gender = $bday = "";
    $nameErr = $lnameErr = $emailErr = $genderErr = $websiteErr = $bdayErr = "";
    
    if (isset($_GET['name'])) { $name = $_GET['name']; }
    if (isset($_GET['lname'])) { $lname = $_GET['lname']; }
    if (isset($_GET['email'])) { $email = $_GET['email']; }
    if (isset($_GET['website'])) { $website = $_GET['website']; }
    if (isset($_GET['bday'])) { $bday = $_GET['bday']; }
    if (isset($_GET['comment'])) { $comment = $_GET['comment']; }
    if (isset($_GET['gender'])) { $gender = $_GET['gender']; }

    if (isset($_GET['nameErr'])) { $nameErr = $_GET['nameErr']; }
    if (isset($_GET['lnameErr'])) { $lnameErr = $_GET['lnameErr']; }
    if (isset($_GET['emailErr'])) { $emailErr = $_GET['emailErr']; }
    if (isset($_GET['websiteErr'])) { $websiteErr = $_GET['websiteErr']; }
    if (isset($_GET['bdayErr'])) { $bdayErr = $_GET['bdayErr']; }
    if (isset($_GET['commentErr'])) { $commentErr = $_GET['commentErr']; }
    if (isset($_GET['genderErr'])) { $genderErr = $_GET['genderErr']; }

    
?>


<h2>PHP Form Validation Example</h2>
<p><span class="error">* required field.</span></p>
<form method="post" action="register2_php.php">  
  Name: <input type="text" name="name" value="<?php echo $name;?>">
  <span class="error">* <?php echo $nameErr;?></span>
  <br><br>
  Last name: <input type="text" name="lname" value="<?php echo $lname;?>">
  <span class="error">* <?php echo $lnameErr;?></span>
  <br><br>
  E-mail: <input type="text" name="email" value="<?php echo $email;?>">
  <span class="error">* <?php echo $emailErr;?></span>
  <br><br>
  Website: <input type="text" name="website" value="<?php echo $website;?>">
  <span class="error"><?php echo $websiteErr;?></span>
  <br><br>
  Date of birth: <input type="date" name="bday" value="<?php echo $bday;?>">
  <span class="error">* <?php echo $bdayErr;?></span>
  <br><br>
  Comment: <textarea name="comment" rows="5" cols="40"><?php echo $comment;?></textarea>
  <br><br>
   Gender:
  <input type="radio" id="female" name="gender" <?php if (isset($gender) && $gender=="female") echo "checked";?> value="female"><label for="female">Female</label>
  <input type="radio" id="male" name="gender" <?php if (isset($gender) && $gender=="male") echo "checked";?> value="male"><label  for="male">Male</label>
  <span class="error">* <?php echo $genderErr;?></span>
  <br><br>
  <input class="submit" type="submit" name="submit" value="Submit">  
</form>

</body>
</html>